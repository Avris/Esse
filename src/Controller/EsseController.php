<?php

namespace Avris\Esse\Controller;

use Avris\Esse\Service\Esse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\String\AbstractString;

use function Symfony\Component\String\u;

final class EsseController
{
    private Esse $esse;
    private AbstractString $publicImagesDir;

    public function __construct(Esse $esse, string $publicImagesDir)
    {
        $this->esse = $esse;
        $this->publicImagesDir = $this->esse->ensureDir($publicImagesDir);
    }

    public function image(string $filename, string $size, string $extension): Response
    {
        $size = u($size)->trim()->lower();
        $extension = u($extension)->trim()->lower();

        $image = $this->esse->getImage($filename);
        if (!$image || !$image->published()) {
            throw new NotFoundHttpException();
        }

        $img = $this->esse->generateImage($image, $size, $extension);
        if (!$img) {
            throw new NotFoundHttpException();
        }

        [$mime, $content] = $img;

        file_put_contents(
            sprintf('%s/%s_%s.%s', $this->publicImagesDir, $filename, $size, $extension),
            $content
        );

        return new Response($content, 200, ['content-type' => $mime]);
    }

    public function file(string $filename, string $extension): Response
    {
        $file = $this->esse->getFile($filename);
        if (!$file || !$file->published() || $filename . '.' . $extension !== $file->filename()->toString()) {
            throw new NotFoundHttpException();
        }

        return new BinaryFileResponse($this->esse->filesDir()->append('/')->append($file->filename()));
    }
}
