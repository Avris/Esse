<?php

namespace Avris\Esse\Entity;

use Symfony\Component\String\AbstractString;

class Image extends File implements \JsonSerializable
{
    public function alt(): ?string
    {
        return $this->meta('alt');
    }

    public function source(): ?string
    {
        return $this->meta('source');
    }

    public function urlForSize(string $size): AbstractString
    {
        return $this->type()->append('/')
            ->append($this->filename()->beforeLast('.'))
            ->append('_')->append($size)
            ->append('.')->append($this->extension());
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'url' => $this->urlForSize('<size>'),
        ]);
    }
}
