<?php

namespace Avris\Esse\Entity;

use Symfony\Component\String\AbstractString;

use function Symfony\Component\String\u;

class File extends Entry implements \JsonSerializable
{
    private AbstractString $filename;
    private AbstractString $basename;
    private AbstractString $extension;

    public function __construct(string $key, array $data)
    {
        parent::__construct($key, $data);
        $this->filename = u($data['filename']);
        unset($data['filename']);
        $this->basename = $this->filename->prepend('/')->afterLast('/');
        $this->extension = $this->basename->afterLast('.')->lower();
    }

    public function filename(): AbstractString
    {
        return $this->filename;
    }

    public function basename(): AbstractString
    {
        return $this->basename;
    }

    public function extension(): AbstractString
    {
        return $this->extension;
    }

    public function url(): AbstractString
    {
        return $this->type()->append('/')->append($this->filename);
    }

    public function title(): ?string
    {
        return $this->meta('title');
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'filename' => $this->filename,
            'basename' => $this->basename,
            'extension' => $this->extension,
            'path' => $this->path(),
            'url' => $this->url(),
        ]);
    }
}
