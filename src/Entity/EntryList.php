<?php

namespace Avris\Esse\Entity;

final class EntryList implements \IteratorAggregate, \Countable, \JsonSerializable
{
    /** @var Entry[] */
    private array $entries = [];

    public function append(Entry $entry): self
    {
        $newList = new self();
        $newList->entries = $this->entries;
        $newList->entries[$entry->key()->toString()] = $entry;

        return $newList;
    }

    public function filter(callable $fn): self
    {
        $newList = new self();
        $newList->entries = array_filter($this->entries, $fn);

        return $newList;
    }

    public function sort(callable $fn): self
    {
        $newList = new self();
        $newList->entries = $this->entries;
        uasort($newList->entries, $fn);

        return $newList;
    }

    public function limit(int $offset, int $length): self
    {
        $newList = new self();
        $newList->entries = array_slice($this->entries, $offset, $length);

        return $newList;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->entries);
    }

    public function count(): int
    {
        return count($this->entries);
    }

    public function toArray(): array
    {
        return $this->entries;
    }

    public function jsonSerialize(): array
    {
        return $this->entries;
    }
}
