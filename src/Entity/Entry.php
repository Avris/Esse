<?php

namespace Avris\Esse\Entity;

use Symfony\Component\String\AbstractString;

use function Symfony\Component\String\u;

class Entry implements \JsonSerializable
{
    private AbstractString $key;
    private AbstractString $type;
    private bool $published;
    private array $meta;
    private array $content;

    public function __construct(string $key, array $data)
    {
        $this->key = u($key);

        $this->content = $data['content'] ?? [];
        unset($data['content']);

        $this->type = u($data['type'] ?? 'block');
        unset($data['type']);

        $this->published = $data['published'] ?? true;
        unset($data['published']);

        $this->meta = $data;
    }

    public function key(): AbstractString
    {
        return $this->key;
    }

    public function keyParts(): array
    {
        return $this->key->split('/');
    }

    public function published(): bool
    {
        return $this->published;
    }

    public function meta(string $key, $default = null)
    {
        return $this->meta[$key] ?? $default;
    }

    public function allMeta(): array
    {
        return $this->meta;
    }

    public function type(): AbstractString
    {
        return $this->type;
    }

    public function content(string $locale, $default = null)
    {
        return $this->content[$locale] ?? $default;
    }

    public function versions(): array
    {
        return array_keys($this->content);
    }

    public function with(array $data): self
    {
        return new static($this->key, $data + ['type' => $this->type, 'published' => $this->published]);
    }

    public function jsonSerialize(): array
    {
        return [
            'key' => $this->key,
            'type' => $this->type(),
            'published' => $this->published,
            'meta' => $this->meta,
            'content' => $this->content,
        ];
    }
}
