<?php

namespace Avris\Esse\Interfaces;

use Avris\Esse\Entity\Entry;

interface EsseModifier
{
    public function modifyEntry(Entry $entry): ?Entry;
}
