<?php

namespace Avris\Esse\Interfaces;

interface EsseIndex
{
    public function id(): string;

    public function build(iterable $rawFiles) : array;
}
