<?php

namespace Avris\Esse\Service;

use Avris\Esse\Entity\Entry;
use Avris\Esse\Entity\EntryList;
use Avris\Esse\Entity\File;
use Avris\Esse\Entity\Image;
use Avris\Esse\Interfaces\EsseIndex;
use Avris\Esse\Interfaces\EsseModifier;
use Avris\Suml\Suml;
use Avris\Suml\Exception\RuntimeException;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\ChainAdapter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\AbstractString;
use Symfony\Contracts\Cache\CacheInterface;

use function Symfony\Component\String\u;

final class Esse
{
    public const TYPE_BLOCK = 'block';
    public const TYPE_IMAGE = 'image';
    public const TYPE_FILE = 'file';

    private AbstractString $namespace;
    private AbstractString $entriesDir;
    private AbstractString $imagesDir;
    private AbstractString $filesDir;
    private CacheInterface $cache;
    private CacheInterface $memoryCache;
    private Suml $suml;
    /** @var EsseModifier[] */
    private iterable $modifiers;
    /** @var EsseIndex[] */
    private iterable $indexes;
    private array $imageSizes;
    private string $locale;
    /** @var string[] */
    private array $locales;

    public function __construct(
        string $namespace,
        string $entriesDir,
        string $imagesDir,
        string $filesDir,
        string $env,
        CacheInterface $cache,
        iterable $modifiers,
        iterable $indexes,
        array $imageSizes,
        RequestStack $requestStack,
        string $fallbackLocale
    )
    {
        $this->namespace = u(basename($namespace));
        $this->entriesDir = $this->ensureDir($entriesDir);
        $this->imagesDir = $this->ensureDir($imagesDir);
        $this->filesDir = $this->ensureDir($filesDir);

        $cacheAdapters = [new ArrayAdapter()];
        if ($env !== 'dev') {
            $cacheAdapters[] = $cache;
        }
        $this->cache = new ChainAdapter($cacheAdapters);
        $this->memoryCache = new ArrayAdapter();

        $this->suml = new Suml();
        $this->modifiers = $modifiers;
        $this->indexes = $indexes;
        $this->imageSizes = $imageSizes;
        $request = $requestStack->getCurrentRequest();
        if (!$request) {
            $this->locale = $fallbackLocale;
            $this->locales = [$fallbackLocale, '_'];
            return;
        }
        $this->locale = $request->getLocale();
        $this->locales = array_unique([$this->locale, $fallbackLocale, '_']);
    }

    public function ensureDir(string $dir): AbstractString
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true)) {
                throw new \RuntimeException(sprintf('Cannot create directory %s', $dir));
            }
        }

        return u(realpath($dir));
    }

    public function entriesDir(): AbstractString
    {
        return $this->entriesDir;
    }

    public function imagesDir(): AbstractString
    {
        return $this->imagesDir;
    }

    public function filesDir(): AbstractString
    {
        return $this->filesDir;
    }

    public function getList(string $prefix = '', ?string $type = null)
    {
        $dir = $this->entriesDir . '/' . $prefix;

        $entries = new EntryList();

        /** @var \SplFileInfo $file */
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir)) as $file) {
            if ($file->isDir() || $file->getExtension() !== 'suml'){
                continue;
            }

                $key = mb_substr($file->getRealPath(), mb_strlen($this->entriesDir) + 1, -5);

            $entry = $this->get($key);
            if ($entry && (!$type || $entry->type()->equalsTo($type))) {
                $entries = $entries->append($entry);
            }
        }

        return $entries;
    }

    public function get(string $key): ?Entry
    {
        return $this->cache->get(
            $this->cacheKey('entry', $key),
            function () use ($key) {
                try {
                    $data = $this->suml->parseFile($this->entriesDir->append('/')->append($key)->append('.suml'));
                } catch (RuntimeException $e) {
                    $data = $this->suml->parseFile($this->entriesDir->append('/')->append($key)->append('/_.suml'));
                }

                $entry = $this->buildEntry($key, null, $data);

                foreach ($this->modifiers as $modifier) {
                    $entry = $modifier->modifyEntry($entry);
                    if (!$entry) {
                        return null;
                    }
                }

                return $entry;
            }
        );
    }

    public function getPart(string $key, array $replacements = [])
    {
        $parts = u($key)->split('.');
        $entryKey = array_shift($parts);

        $content = $this->cache->get(
            $this->cacheKey('entry-part--' . $this->locale, $key),
            function () use ($entryKey, $parts, $replacements) {
                $entry = $this->get($entryKey);

                $content = $this->memoryCache->get(
                    $this->cacheKey('entry-part-merge-' . join('-', $this->locales), $entryKey),
                    function () use ($entry) {
                        return $this->merge(
                            array_map(function ($locale) use ($entry) {
                                return $entry->content($locale);
                            }, $this->locales)
                        );
                    }
                );

                foreach ($parts as $part) {
                    $content = $content[$part->toString()] ?? null;
                }

                return $content;
            }
        );

        foreach ($replacements as $key => $value) {
            $content = str_replace('%' . $key . '%', $value, $content);
        }

        return $content;
    }

    private function merge($arrays = [])
    {
        $arr = array_values(array_filter($arrays, function ($a) {
            return is_array($a);
        }));

        if (count($arr) === 1) {
            return $arr[0];
        } elseif (count($arr) === 0) {
            return $arrays[0];
        }

        $newArray = [];

        foreach ($arr as $a) {
            foreach ($a as $k => $v) {
                $newArray[$k] = $this->merge([
                    $v,
                    $newArray[$k] ?? null
                ]);
            }
        }

        return $newArray;
    }

    public function getImage(string $key): ?Image
    {
        try {
            $image = $this->buildEntry($key, 'image', $this->suml->parseFile($this->imagesDir . '/' . $key . '.suml'));

            foreach ($this->modifiers as $modifier) {
                $image = $modifier->modifyEntry($image);
                if (!$image) {
                    return null;
                }
            }

            return $image;
        } catch (RuntimeException $e) {
            return null;
        }
    }

    public function getFile(string $key): ?File
    {
        try {
            $file = $this->buildEntry($key, 'file', $this->suml->parseFile($this->filesDir . '/' . $key . '.suml'));

            foreach ($this->modifiers as $modifier) {
                $file = $modifier->modifyEntry($file);
                if (!$file) {
                    return null;
                }
            }

            return $file;
        } catch (RuntimeException $e) {
            return null;
        }
    }

    public function fromIndex(string $id, string $key)
    {
        $index = $this->fullIndex($id);

        return $index[$key] ?? null;
    }

    public function allEntryFilesRaw(): iterable
    {
        /** @var \SplFileInfo $file */
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($this->entriesDir)) as $file) {
            if ($file->isDir() || $file->getExtension() !== 'suml'){
                continue;
            }

            $key = u($file->getRealPath())->after($this->entriesDir)->replace('\\', '/')->trimStart('/')->beforeLast('.suml');

            yield $key->toString() => $this->suml->parseFile($file->getRealPath());
        }
    }

    public function fullIndex(string $id): array
    {
        foreach ($this->indexes as $index) {
            if ($index->id() !== $id) {
                continue;
            }

            return $this->cache->get(
                $this->cacheKey('index', $id),
                function () use ($index) {
                    return $index->build($this->allEntryFilesRaw());
                }
            );
        }

        return [];
    }

    private function cacheKey(string $type, string $key): string
    {
        return sprintf(
            '%s--esse--%s--%s',
            $this->namespace,
            $type,
            str_replace('/', '|', $key)
        );
    }

    public function generateImage(Image $image, string $size, string $outputType): ?array
    {
        $path = $this->imagesDir->append('/')->append($image->filename());
        $info = getimagesize($path);
        if (!$info) {
            return null;
        }
        [$width, $height, $type] = $info;

        switch ($type) {
            case IMAGETYPE_JPEG:
                $img = imagecreatefromjpeg($path);
                break;
            case IMAGETYPE_GIF:
                $img = imagecreatefromgif($path);
                break;
            case IMAGETYPE_PNG:
                $img = imagecreatefrompng($path);
                break;
            default:
                return null;
        }

        $newSize = $this->determineNewImageSize($size, $width, $height);
        if (!$newSize) {
            return null;
        }
        [$targetWidth, $targetHeight] = $newSize;

        imagealphablending($img, true);
        imagesavealpha($img, true);
        $newImg = $this->resizeImage($img, $width, $height, $targetWidth, $targetHeight);

        switch ($outputType) {
            case 'jpeg':
            case 'jpg':
                ob_start();
                imagejpeg($newImg);
                return ['image/jpeg', ob_get_clean()];
            case 'png':
                ob_start();
                imagepng($newImg);
                return ['image/png', ob_get_clean()];
            case 'gif':
                ob_start();
                imagegif($newImg);
                return ['image/gif', ob_get_clean()];
            default:
                return null;
        }
    }

    public function determineNewImageSize(string $size, int $width, int $height): ?array
    {
        $targetSize = $this->imageSizes[$size] ?? null;
        if (!$targetSize) {
            return null;
        }

        $targetWidth = $width;
        $targetHeight = $height;
        $ratio = $targetWidth / $targetHeight;

        if (($targetSize['maxwidth'] ?? null) && $targetWidth > $targetSize['maxwidth']) {
            $targetWidth = $targetSize['maxwidth'];
            $targetHeight = $targetWidth / $ratio;
        }

        if (($targetSize['maxheight'] ?? null) && $targetHeight > $targetSize['maxheight']) {
            $targetHeight = $targetSize['maxheight'];
            $targetWidth = $targetWidth * $ratio;
        }

        return [$targetWidth, $targetHeight];
    }

    private function resizeImage($img, int $originalWidth, int $originalHeight, int $targetWidth, int $targetHeight)
    {
        $newImg = imagecreatetruecolor($targetWidth, $targetHeight);
        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);

        imagecopyresampled($newImg, $img, 0, 0, 0, 0, $targetWidth, $targetHeight, $originalWidth, $originalHeight);

        return $newImg;
    }

    private function buildEntry(string $key, ?string $type, array $data): Entry
    {
        switch ($type ?? $data['type'] ?? null) {
            case self::TYPE_IMAGE:
                return new Image($key, ['type' => self::TYPE_IMAGE] + $data);
            case self::TYPE_FILE:
                return new File($key, ['type' => self::TYPE_FILE] + $data);
            default:
                return new Entry($key, $data);
        }
    }
}
