<?php

namespace Avris\Esse\Service;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class EsseTwigExtension extends AbstractExtension
{
    private Esse $esse;

    public function __construct(Esse $esse)
    {
        $this->esse = $esse;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('esse', [$this->esse, 'getPart'], ['is_safe' => ['html']]),
        ];
    }
}
