<?php

namespace Avris\Esse\DependencyInjection;

use Avris\Esse\Service\Esse;
use Avris\Esse\Interfaces\EsseIndex;
use Avris\Esse\Interfaces\EsseModifier;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class AvrisEsseExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $container
            ->registerForAutoconfiguration(EsseModifier::class)
            ->addTag('esse.modifier');

        $container
            ->registerForAutoconfiguration(EsseIndex::class)
            ->addTag('esse.index');

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->getDefinition(Esse::class)->replaceArgument('$entriesDir', $config['entriesDir']);
        $container->getDefinition(Esse::class)->replaceArgument('$imagesDir', $config['imagesDir']);
        $container->getDefinition(Esse::class)->replaceArgument('$filesDir', $config['filesDir']);

        $container->getDefinition(Esse::class)->replaceArgument('$imageSizes', $config['imageSizes']);
    }
}
