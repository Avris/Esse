<?php

namespace Avris\Esse\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('avris_esse');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('entriesDir')
                    ->defaultValue('%kernel.project_dir%/content/entries')
                ->end()
                ->scalarNode('imagesDir')
                    ->defaultValue('%kernel.project_dir%/content/images')
                ->end()
                ->scalarNode('filesDir')
                    ->defaultValue('%kernel.project_dir%/content/files')
                ->end()
                ->arrayNode('imageSizes')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('maxwidth')->defaultNull()->end()
                            ->scalarNode('maxheight')->defaultNull()->end()
                        ->end()
                    ->end()
                    ->defaultValue([
                        'big' => ['maxwidth' => 960, 'maxheight' => null],
                        'small' => ['maxwidth' => 480, 'maxheight' => null],
                        'micro' => ['maxwidth' => 36, 'maxheight' => 36],
                    ])
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
